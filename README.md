
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin)  | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# Statistical learning class

- Omar Trejo
- October, 2014

Statistical learning code developed with R for the statistical learning class
that I attended at ITAM.

The main resource for the class was:

- [Hastie, Tibshirani & Friedman - The Elements of Statistical Learning (2009)](http://statweb.stanford.edu/~tibs/ElemStatLearn/)

---

> "The best ideas are common property."
>
> —Seneca
