MM2IM <- function(mm) {
    #
    # Model matrix to image matrix
    #
    # Omar Trejo Navarro
    # December, 2014
    #
    width  <- max(mm[, 2])
    height <- max(mm[, 3])
    im     <- array(NA, dim = c(width, height))
    mm     <- data.frame(mm)
    for (i in 1:width) {
        for (j in 1:height) {
            idx1 <- which(mm[, 2] == i)
            idx2 <- which(mm[idx1, 3] == j)
            im[i, j] <- mm[idx1, 1][idx2]
        }
    }
    return(im)
}
